import curses
import random
import time

# Set non-ascii font size to 61

class State:
    def __init__(self, row, col, emoji):
        self.row = row
        self.col = col
        self.emoji = emoji
        self.matched = False
        self.selected = False


def print_cards(stdscr, rows, cols, states):
    offset_rows = rows//2 - 10
    offset_cols = cols//2 - 22

    for state in states:
        if state.matched:
            stdscr.addstr(state.row + offset_rows, state.col + offset_cols, '┌──────┐')
            stdscr.addstr(state.row + offset_rows + 1, state.col + offset_cols, '│      │')
            stdscr.addstr(state.row + offset_rows + 2, state.col + offset_cols, f'│      │')
            stdscr.addstr(state.row + offset_rows + 3, state.col + offset_cols, f'│{state.emoji}    │')
            stdscr.addstr(state.row + offset_rows + 4, state.col + offset_cols, '└──────┘')
        elif state.selected:
            stdscr.addstr(state.row + offset_rows, state.col + offset_cols, '┌──────┐')
            stdscr.addstr(state.row + offset_rows + 1, state.col + offset_cols, '│      ▐')
            stdscr.addstr(state.row + offset_rows + 2, state.col + offset_cols, f'│      ▐')
            stdscr.addstr(state.row + offset_rows + 3, state.col + offset_cols, f'│{state.emoji}    ▐')
            stdscr.addstr(state.row + offset_rows + 4, state.col + offset_cols, '└▄▄▄▄▄▄▟')
        else:
            stdscr.addstr(state.row + offset_rows, state.col + offset_cols, '┌──────┐')
            stdscr.addstr(state.row + offset_rows + 1, state.col + offset_cols, '│      │')
            stdscr.addstr(state.row + offset_rows + 2, state.col + offset_cols, '│      │')
            stdscr.addstr(state.row + offset_rows + 3, state.col + offset_cols, '│      │')
            stdscr.addstr(state.row + offset_rows + 4, state.col + offset_cols, '└──────┘')

def get_selection(stdscr, states, rows, cols):
    offset_rows = rows//2 - 10
    offset_cols = cols//2 - 22

    while 1:
        ch = stdscr.getch()

        if ch == curses.KEY_MOUSE:
            _, col, row, _, _ = curses.getmouse()

            for state in states:
                if state.row + offset_rows < row and state.row + offset_rows + 5 >= row and state.col + offset_cols < col and state.col + offset_cols + 8 >= col and not state.matched and not state.selected:
                    state.selected = True
                    return state

def check_win(states):
    for state in states:
        if not state.matched:
            return False
    return True

def print_win(stdscr):
    stdscr.addstr(1, 0, 'You win!')

def print_count(stdscr, count):
    stdscr.addstr(0, 0, str(count))

def get_emojis():
    return ['😈', '🤡', '💩', '👻', '👽', '🤖', '😀', '😻', '👹', '🤢']

def get_states():
    emojis = get_emojis() + get_emojis()
    random.shuffle(emojis)

    states = []
    for i in range(4):
        for j in range(5):
            states.append(State(i * 5, j * 9, emojis.pop()))

    return states

def init_curses():
    curses.curs_set(0)
    curses.mousemask(1)
    curses.start_color()
    curses.use_default_colors()

def main(stdscr):
    init_curses()
    rows, cols = stdscr.getmaxyx()
    states = get_states()
    count = 0
    print_cards(stdscr, rows, cols, states)
    print_count(stdscr, 0)

    while 1:
        state1 = get_selection(stdscr, states, rows, cols)
        print_cards(stdscr, rows, cols, states)

        state2 = get_selection(stdscr, states, rows, cols)
        print_cards(stdscr, rows, cols, states)

        count += 1
        print_count(stdscr, count)

        stdscr.refresh()
        time.sleep(0.75)

        if state1.emoji == state2.emoji:
            state1.matched = True
            state2.matched = True
        else:
            state1.selected = False
            state2.selected = False

        print_cards(stdscr, rows, cols, states)

        if check_win(states):
            print_win(stdscr)
            stdscr.getch()
            break

if __name__ == '__main__':
    curses.wrapper(main)

